Kiss-CapacityLimiter
--------------------

Status:
-----------

Works to transparently reserve a certain amount of device memory to approximate a GPU with smaller memory capacity.

It should be based on public technology and be freely distributable.

Using LD_PRELOAD and wrapping main and exit, it:
(1) print a helpful header
(2) allocates "RESERVED" memory (environment variable), in MB, using cudaMalloc
(3) frees the memory at the program exit

Building:
------------

> make

Using:
---------

> cd test-apps/bigmem
> LD_PRELOAD=../../libcapacitylimiter.so ./bigmem   # should pass
> RESERVE=1000 LD_PRELOAD=../../libcapacitylimiter.so ./bigmem   # should fail 

Testing:
----------

(Requires the bats Bash testing framework to be installed.)
> bats test.bats

Limitations:
-------------

LD_PRELOAD does not follow jobs that are launched through a Makefile.
