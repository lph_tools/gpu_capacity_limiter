#include <parse_ptx.h>

// global variables
bool ALREADY_PARSED_PTX;
std::set<std::string> PARSED_CUBINS;

// parsed variables
std::string version_str;
std::string target_str;
std::string address_size_str;
// name -> code mapping
std::unordered_map<std::string, std::string> kernels_ptx;
// std::unordered_map<std::string, std::string> kernels_compiled;

void init_ptx_parser(void) {
    // initialize the global variables
    ALREADY_PARSED_PTX = false;
    PARSED_CUBINS = std::set<std::string>();
}

int parsed_cubins() {
    // count the number of cubin files that have been parsed
    return PARSED_CUBINS.size();
}

template <class Container>
void split(const std::string& str, Container& cont, char delim='\n') {
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, delim)) {
        cont.push_back(token);
    }
}

bool begins_with(const std::string &input, const std::string &s) {
    return (input.substr(0, s.size()) == s);
}

std::string last_word_without_parens (const std::string &s) {
    std::string::size_type last_word_idx = s.find_last_of(" ")+1;
    std::string::size_type parens_loc = s.find_last_of("(");
    return s.substr(last_word_idx, parens_loc - last_word_idx);
}

// generate, parse, and store the PTX from a cubin file
bool parse_ptx(std::string cubin_filename) {
    if (ALREADY_PARSED_PTX) {
        // this executable has already been parsed
        return false;
    }

    // parse the cubin file, extracting the PTX for its kernels
    LOG(3, "Parsing PTX from %s\n", cubin_filename.c_str());
    std::string raw_ptx_contents = gen_ptx(cubin_filename);
    std::vector<std::string> split_ptx_contents;
    split(raw_ptx_contents, split_ptx_contents);

    // local control variables for building up each kernel
    bool inside_kernel = false;
    std::string this_kernel_name;
    std::string this_kernel_code;

    for (auto &it: split_ptx_contents) {
        if (begins_with(it, ".version")) {
            version_str = it;
        } else if (begins_with(it, ".target")) {
            target_str = it;
        } else if (begins_with(it, ".address_size")) {
            address_size_str = it;
        } else if (begins_with(it, ".visible")) {
            // sanity check: not inside of a kernel already
            ASSERT(!inside_kernel, "PTX parser found overlapping kernels.");

            // parse the kernel name
            this_kernel_name = last_word_without_parens(it);
            LOG(3, "PTX Parser Beginning Kernel: %s\n", this_kernel_name.c_str());

            // start capturing this kernel
            inside_kernel = true;
            this_kernel_code = it;
            this_kernel_code += "\n";

        } else if (begins_with(it, "}")) {
            LOG(3, "PTX Parser Ending Kernel: %s\n", this_kernel_name.c_str());

            // stop capturing the kernel
            this_kernel_code += it;
            this_kernel_code += "\n";
            inside_kernel = false;

            // place the kernel in the global map
            bool kernel_not_found = kernels_ptx.find(this_kernel_name) == kernels_ptx.end();
            ASSERT(kernel_not_found, "Kernel %s already exists!", this_kernel_name.c_str())
            kernels_ptx.insert(std::make_pair(this_kernel_name, this_kernel_code));

        } else if (inside_kernel) {
            // build up each kernel line by line
            this_kernel_code += it;
            this_kernel_code += "\n";
        }
    }

    // debugging
    print_ptx(4);  // very low priority

    // register the cubin as parsed
    ALREADY_PARSED_PTX = true;
    PARSED_CUBINS.insert(cubin_filename);
    return true;
}

// from https://stackoverflow.com/a/3578548/5849397
std::string get_stdout_from_command(std::string cmd, bool stderr=true) {

    std::string data;
    FILE * stream;
    const int max_buffer = 256;
    char buffer[max_buffer];
    if (stderr) {  // also capture stderr
        cmd.append(" 2>&1");
    }

    stream = popen(cmd.c_str(), "r");
    ASSERT(stream, "parse_ptx.cpp, getting output from shell failed!\n");
    while (!feof(stream))
        if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
    pclose(stream);

    return data;
}

std::string gen_ptx(std::string cubin_filename) {
    std::ostringstream cmd_ss;
    cmd_ss << "cuobjdump -ptx " << cubin_filename;
    return get_stdout_from_command(cmd_ss.str());
}

void print_ptx(int log_level) {
    for(auto &kernel_pair: kernels_ptx) {
        LOG(log_level, "Parsed Kernel PTX: %s", kernel_pair.first.c_str());
        // NOTE: the kernel code is likely to be longer than log.h's
        //       MAX_VERBOSE_STRING, so we make our own formatted output
        if (VERBOSITY >= log_level) {
            std::cout << kernel_pair.second.c_str();
        }
    }
}

// append the version, target, address_size to make a compilable PTX function
std::string form_compilable_kernel(std::string kernel_name, std::string kernel_contents) {
    std::ostringstream compilable_kernel;
    compilable_kernel << version_str << std::endl;
    compilable_kernel << target_str << std::endl;
    compilable_kernel << address_size_str << std::endl;
    compilable_kernel << std::endl << std::endl << std::endl;

    compilable_kernel << kernels_ptx[kernel_name];
    compilable_kernel << std::endl;
    return compilable_kernel.str();
}

const char *vecadd_cuobj_ptx = "                                \n\
.version 5.0                                                    \n\
.target sm_35                                                   \n\
.address_size 64                                                \n\
                                                                \n\
                                                                \n\
                                                                \n\
.visible .entry _Z6vecAddPdS_S_i(                               \n\
.param .u64 _Z6vecAddPdS_S_i_param_0,                           \n\
.param .u64 _Z6vecAddPdS_S_i_param_1,                           \n\
.param .u64 _Z6vecAddPdS_S_i_param_2,                           \n\
.param .u32 _Z6vecAddPdS_S_i_param_3                            \n\
)                                                               \n\
{                                                               \n\
.reg .pred %p<2>;                                               \n\
.reg .b32 %r<6>;                                                \n\
.reg .f64 %fd<4>;                                               \n\
.reg .b64 %rd<11>;                                              \n\
                                                                \n\
                                                                \n\
ld.param.u64 %rd1, [_Z6vecAddPdS_S_i_param_0];                  \n\
ld.param.u64 %rd2, [_Z6vecAddPdS_S_i_param_1];                  \n\
ld.param.u64 %rd3, [_Z6vecAddPdS_S_i_param_2];                  \n\
ld.param.u32 %r2, [_Z6vecAddPdS_S_i_param_3];                   \n\
mov.u32 %r3, %ctaid.x;                                          \n\
mov.u32 %r4, %ntid.x;                                           \n\
mov.u32 %r5, %tid.x;                                            \n\
mad.lo.s32 %r1, %r4, %r3, %r5;                                  \n\
setp.ge.s32    %p1, %r1, %r2;                                   \n\
@%p1 bra BB0_2;                                                 \n\
                                                                \n\
cvta.to.global.u64 %rd4, %rd1;                                  \n\
mul.wide.s32 %rd5, %r1, 8;                                      \n\
add.s64 %rd6, %rd4, %rd5;                                       \n\
cvta.to.global.u64 %rd7, %rd2;                                  \n\
add.s64 %rd8, %rd7, %rd5;                                       \n\
ld.global.f64 %fd1, [%rd8];                                     \n\
ld.global.f64 %fd2, [%rd6];                                     \n\
add.f64 %fd3, %fd2, %fd1;                                       \n\
cvta.to.global.u64 %rd9, %rd3;                                  \n\
add.s64 %rd10, %rd9, %rd5;                                      \n\
st.global.f64 [%rd10], %fd3;                                    \n\
                                                                \n\
BB0_2:                                                          \n\
ret;                                                            \n\
}                                                               \n";

// compile an extracted PTX kernel using NVRTC
void compile_ptx(std::string kernel_name) {
    // get the kernel PTX
    bool kernel_not_found = kernels_ptx.find(kernel_name) == kernels_ptx.end();
    if(kernel_not_found) {
        LOG(-1, "Parsed kernels:\n");
        for(auto &kernel_pair: kernels_ptx) {
            LOG(-1, "%s\n", kernel_pair.first.c_str());
        }
        ERROR(kernel_not_found, "Kernel %s is not parsed!", kernel_name.c_str())
    }
    std::string kernel_contents = kernels_ptx[kernel_name];

    // Create an instance of nvrtcProgram
    CUdevice cuDevice;
    CUcontext context;
    CUmodule module;
    CUfunction kernel;
    CUDA_SAFE_CALL(cuInit(0));
    CUDA_SAFE_CALL(cuDeviceGet(&cuDevice, 0));
    CUDA_SAFE_CALL(cuCtxCreate(&context, 0, cuDevice));

    // Compile this kernel
    // NOTE: this cuts off the beginning of the kernel. Why?!
    const char *kern = form_compilable_kernel(kernel_name, kernel_contents).c_str();
    CUDA_SAFE_CALL(cuModuleLoadDataEx(&module, form_compilable_kernel(kernel_name, kernel_contents).c_str(), 0, 0, 0));

    // debugging --- dump out the two versions of the function
    /* FILE *fp0 = fopen("./ver_hardcode.txt", "w"); */
    /* if (fp0 != NULL) { */
    /*     fputs(vecadd_cuobj_ptx, fp0); */
    /*     fclose(fp0); */
    /* } */
    /* FILE *fp1 = fopen("./ver_derived.txt", "w"); */
    /* if (fp1 != NULL) { */
    /*     fputs(form_compilable_kernel(kernel_name, kernel_contents).c_str(), fp1); */
    /*     fclose(fp1); */
    /* } */

    /* CUDA_SAFE_CALL(cuModuleLoadDataEx(&module, vecadd_cuobj_ptx, 0, 0, 0)); */
    CUDA_SAFE_CALL(cuModuleGetFunction(&kernel, module, kernel_name.c_str()));
}
