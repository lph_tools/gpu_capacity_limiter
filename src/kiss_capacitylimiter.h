// MIT License
// 
// Copyright (c) 2019 NVIDIA
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Author(s) : Michael Sullivan
//			   Esha Choukse
//			  
#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <dlfcn.h>
#include <assert.h>

#include "log.h"
#include "globals.h"

// for data sizes
const uint64_t KILO = 1024;
const uint64_t MEGA = KILO*KILO;

// our reserved space
uint64_t *reserved_space;

// for debugging
void check_cuda_error(uint32_t lineno, std::string identifier) {
    cudaError_t err = cudaGetLastError();
    if (err != cudaSuccess) {
        std::cerr
            << "Cuda Error (" << lineno << "-" << identifier << "): "
            << cudaGetErrorString(err)
            << std::endl;
            exit(err);
    }
}

// at end
void lib_term(void);
