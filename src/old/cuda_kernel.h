#pragma once

#include <vector>
#include <assert.h>

#include <cuda.h>
#include <cuda_runtime.h>

#include "globals.h"
#include "log.h"

#define max_threads_per_block 1024   // for sm_2x to sm_62 (https://en.wikipedia.org/wiki/CUDA)

// for holding kernel information

class CudaKernel {
    public:
    dim3 gridDim;
    dim3 blockDim;
    size_t sharedMem;
    cudaStream_t stream;
    int configure_count;
    std::vector<void *> args;
    std::vector<size_t> sizes;
    std::vector<size_t> offsets;
    void configure(dim3 gridDim, dim3 blockDim, size_t sharedMem, cudaStream_t stream);  // registers the launch parameters 
    void setup_argument(const void *arg, size_t size, size_t offset);  // adds a kernel argument to the stack
    void launch();  // just sanity checks and clears itself
    void print_kernel_info(int log_level);
    CudaKernel() : configure_count(0) {}
};
