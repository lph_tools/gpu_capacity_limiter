#include "cuda_kernel.h"

void CudaKernel::configure(dim3 this_gridDim, dim3 this_blockDim, size_t this_sharedMem, cudaStream_t this_stream) {

   LOG(4, "In CudaKernel::configure, configuring a kernel.\n"); 

    gridDim = this_gridDim;
    dim3 adj_blockDim = { 2*this_blockDim.x, this_blockDim.y, this_blockDim.z };
    uint32_t total_adj_blockSize = adj_blockDim.x * adj_blockDim.y * adj_blockDim.z;

    if (GLOBALS::NODOUBLE) {
        blockDim = this_blockDim;
    } else if (total_adj_blockSize > max_threads_per_block) {
        // thread-level duplication will not work without source rewriting!
        LOG(0, "Thread-Level Duplication Ineligable Program!\n");
        assert(total_adj_blockSize <= max_threads_per_block);
    }
    else {
        LOG(3, "Doubling Number of Threads from (%d, %d, %d) ==> (%d, %d, %d):\n", this_blockDim.x,
                                                                                   this_blockDim.y,
                                                                                   this_blockDim.z,
                                                                                   adj_blockDim.x,
                                                                                   adj_blockDim.y,
                                                                                   adj_blockDim.z);
        blockDim = adj_blockDim;  // double the number of threads
    }

    sharedMem = this_sharedMem;
    stream = this_stream;
    configure_count++;
}

void CudaKernel::setup_argument(const void *arg, size_t size, size_t offset) {
    args.push_back(const_cast<void *>(arg));
    // Q: what to do with sizes and offsets?
    sizes.push_back(size);
    offsets.push_back(offset);
}

void CudaKernel::launch() {
    // sanity check
    bool never_configured = configure_count == 0;
    ERROR(never_configured, "Kernel was never configured!");
    bool multi_configured = configure_count > 1;
    ERROR(multi_configured, "Kernel was configured multiple times!");
}

void CudaKernel::print_kernel_info(int log_level) {
    LOG(log_level, "GridDim: (%d, %d, %d)\n", gridDim.x, gridDim.y, gridDim.z);
    LOG(log_level, "BlockDim: (%d, %d, %d)\n", blockDim.x, blockDim.y, blockDim.z);
    LOG(log_level, "# Args: %d\n", (int)args.size());
}
