#pragma once

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "globals.h"    // for GLOBALS::VERBOSITY

#define MAX_VERBOSE_STRING (4*1024)

#define LOG(level, ...) { \
    if(level <= GLOBALS::VERBOSITY) { \
        char tmp[MAX_VERBOSE_STRING];\
        char * ptmp = tmp;\
        snprintf(ptmp, MAX_VERBOSE_STRING, "[%d] ", level);\
        ptmp += strlen(tmp);\
        snprintf(ptmp, MAX_VERBOSE_STRING-strlen(tmp),  __VA_ARGS__); \
        printf("%s", tmp); \
        fflush(stdout); \
    }\
}

// conditional warning
#define WARN(condition, ...) { \
    if (condition) { \
        char tmp[MAX_VERBOSE_STRING];\
        char * ptmp = tmp;\
        snprintf(ptmp, MAX_VERBOSE_STRING, "[WARNING] ");\
        ptmp += strlen(tmp);\
        snprintf(ptmp, MAX_VERBOSE_STRING-strlen(tmp),  __VA_ARGS__); \
        printf("%s", tmp);\
        fflush(stdout); \
    }\
}

// note: evaluates condition twice, it must agree both times
//       (works fine with simple conditionals)
#define ASSERT(condition, ...) { \
    if (!(condition)) { \
        char tmp[MAX_VERBOSE_STRING];\
        char * ptmp = tmp;\
        snprintf(ptmp, MAX_VERBOSE_STRING, "[ERROR] ");\
        ptmp += strlen(tmp);\
        snprintf(ptmp, MAX_VERBOSE_STRING-strlen(tmp),  __VA_ARGS__); \
        printf("%s", tmp);\
        fflush(stdout); \
        assert((condition)); \
    }\
}

#define ERROR(condition, ...) { \
    if ((condition)) { \
        char tmp[MAX_VERBOSE_STRING];\
        char * ptmp = tmp;\
        snprintf(ptmp, MAX_VERBOSE_STRING, "[ERROR] ");\
        ptmp += strlen(tmp);\
        snprintf(ptmp, MAX_VERBOSE_STRING-strlen(tmp),  __VA_ARGS__); \
        printf("%s", tmp);\
        fflush(stdout); \
        assert((condition)); \
    }\
}
