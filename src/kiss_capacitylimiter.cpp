// MIT License
// 
// Copyright (c) 2019 NVIDIA
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Author(s) : Michael Sullivan
//			   Esha Choukse
//			  
#include "kiss_capacitylimiter.h"

// like "main" for a shared library, called at the very beginning
static void __attribute__ ((constructor)) lib_init(void);
static void lib_init(void) {
    printf("KissCapacityLimiter Starting....\n\n");
    fflush(stdout);

    // Command-Line Arguments
    if(getenv("VERBOSE"))
        GLOBALS::VERBOSITY = atoi(getenv("VERBOSE"));
    if(getenv("RESERVE"))
        GLOBALS::RESERVE = atoi(getenv("RESERVE"));

    /* used the LD_PRELOAD so in case of future calls to system() we don't reload the library */
    unsetenv("LD_PRELOAD");
    printf("   Environment variables:\n\n");
    printf("   VERBOSE              enables verbose messages  (0--4 are levels of verbosity) -  Value: %d\n", GLOBALS::VERBOSITY);
    printf("   RESERVE              reserve X MB of memory to limit the device capacity -  Value: %d\n", (int)GLOBALS::RESERVE);
    printf("------------------------------------------------------------------------------------ \n");

    uint64_t reserve_bytes = (uint64_t)GLOBALS::RESERVE * MEGA;
    cudaMalloc(&reserved_space, reserve_bytes);
    check_cuda_error(__LINE__, "libcapacitylimiter: After cudaMalloc");

    // register the lib_term function
    atexit(lib_term);

    return;
}

// called at the very end
void lib_term() {
    LOG(0, "KissCapacityLimiter Exiting...\n");

    // clean up resources
    cudaFree(reserved_space);
    check_cuda_error(__LINE__, "libcapacitylimiter: After cudaFree");
}
