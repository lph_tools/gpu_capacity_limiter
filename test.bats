#!/usr/bin/env bats

@test "Testing for completion (exit code)" {
    run env LD_PRELOAD=`pwd`/src/libcapacitylimiter.so `pwd`/test-apps/vectoradd/vectoradd
    [ "$status" -eq 0 ]
}

@test "Testing for completion (exit code 2)" {
    run bash -c "LD_PRELOAD=`pwd`/src/libcapacitylimiter.so `pwd`/test-apps/vectoradd/vectoradd"
    [ "$status" -eq 0 ]
}

@test "Testing for completion (header)" {
    run bash -c "LD_PRELOAD=`pwd`/src/libcapacitylimiter.so `pwd`/test-apps/vectoradd/vectoradd | grep CapacityLimiter | wc -l"
    [ "$output" -gt 0 ]
}

@test "Testing for completion (header 2)" {
    head=$(LD_PRELOAD=`pwd`/src/libcapacitylimiter.so `pwd`/test-apps/vectoradd/vectoradd | grep CapacityLimiter | wc -l)
    [ "$head" -gt 0 ]
}

@test "Testing that bigmem works with RESERVE=0" {
    run bash -c "LD_PRELOAD=`pwd`/src/libcapacitylimiter.so `pwd`/test-apps/bigmem/bigmem"
    [ "$status" -eq 0 ]
}

@test "Testing that bigmem fails with RESERVE=1000" {
    run bash -c "RESERVE=1000 LD_PRELOAD=`pwd`/src/libcapacitylimiter.so `pwd`/test-apps/bigmem/bigmem"
    [ "$status" -ne 0 ]
}
