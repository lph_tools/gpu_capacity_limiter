all: libcapacitylimiter test-apps

clean:
	make -C test-apps clean
	make -C src clean

# always try to compile (we don't do dependency checking at this level)
.PHONY:	clean test-apps libcapacitylimiter

libcapacitylimiter:
	make -C src

test-apps:
	make -C test-apps
