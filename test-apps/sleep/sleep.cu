// MIT License
// 
// Copyright (c) 2019 NVIDIA
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Author(s) : Michael Sullivan
//			   Esha Choukse
//			  
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

// for data sizes
const uint64_t KILO = 1024;
const uint64_t MEGA = KILO*KILO;
const uint64_t SLEEP_SECS = 10;
 
// An empty CUDA kernel.
__global__ void null()
{
}
 
int main( int argc, char* argv[] )
{
    int blockSize, gridSize;

    // Number of threads in each thread block
    blockSize = 1024;
 
    // Number of thread blocks in grid
    int n = 100000; 
    gridSize = (int)ceil((float)n/blockSize);
 
    // Execute the kernel
    null<<<gridSize, blockSize>>>();

    std::cout << "Sleeping for " << SLEEP_SECS << " seconds..." << std::endl;
    usleep(SLEEP_SECS * MEGA);  // from unistd.h

    return 0;
}
