// MIT License
// 
// Copyright (c) 2019 NVIDIA
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Author(s) : Michael Sullivan
//			   Esha Choukse
//			  
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

 // for data sizes
 const uint64_t KILO = 1024;
 const uint64_t MEGA = KILO*KILO;
 const uint64_t LEAVE_FREE_MEM = 500;
 const uint64_t SLEEP_SECS = 0;

 void check_cuda_error(uint32_t lineno, std::string identifier) {
     cudaError_t err = cudaGetLastError();
     if (err != cudaSuccess) {
         std::cerr
             << "Cuda Error (" << lineno << "-" << identifier << "): "
             << cudaGetErrorString(err)
             << std::endl;
             exit(err);
     }
 }
 
// An empty CUDA kernel.
__global__ void null()
{
}
 
int main( int argc, char* argv[] )
{
    int blockSize, gridSize;

    // Number of threads in each thread block
    blockSize = 1024;
 
    // Number of thread blocks in grid
    int n = 100000; 
    gridSize = (int)ceil((float)n/blockSize);

    // reserve all device memory
    uint64_t preAvailableMemory, totalMemory;
    uint64_t padMemory = LEAVE_FREE_MEM*MEGA;  // in MB, to avoid out-of-memory errors
    uint64_t reserveSize;
    cudaMemGetInfo(&preAvailableMemory, &totalMemory);
    reserveSize = (totalMemory - padMemory);

    double *d_a;
    cudaMalloc(&d_a, reserveSize);
    check_cuda_error(__LINE__, "After cudaMalloc");

    // Execute the kernel
    null<<<gridSize, blockSize>>>();

    std::cout << "Sleeping for " << SLEEP_SECS << " seconds..." << std::endl;
    usleep(SLEEP_SECS * MEGA);  // from unistd.h

    return 0;
}
